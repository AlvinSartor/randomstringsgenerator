using System.Threading.Tasks;
using NUnit.Framework;

namespace GenerateStrings.NUnit
{
    [TestFixture]
    internal sealed class ParallelizerFixture
    {
        [Test, Repeat(10)]
        public async Task ParallelizerCreatesSetsOfUniqueStrings()
        {
            var result = await Parallelizer.CreateRandomStrings(100, 10);
            Assert.That(result.Count, Is.EqualTo(100));
        }

        [Test]
        public async Task CreateHighNumberOfString()
        {
            const int target = 2_200_000;
            var result = await Parallelizer.CreateRandomStrings(target, 100);
            Assert.That(result.Count, Is.EqualTo(target));
        }
    }
}
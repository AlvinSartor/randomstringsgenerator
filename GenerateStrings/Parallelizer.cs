﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GenerateStrings
{
    public static class Parallelizer
    {
        private static readonly Random Rnd = new Random();

        public static async Task<HashSet<string>> CreateRandomStrings(int totNumber, int numThreads)
        {
            var load = totNumber / numThreads;
            
            var tasks = Enumerable
                .Range(0, numThreads)
                .Select(x => StringGenerator.GenerateBatchOfStringsAsync(new Random(Rnd.Next()),  load));
            
            var executed = (await Task.WhenAll(tasks)).SelectMany(x => x).ToArray();
            var result = executed.ToHashSet();

            // here we fill the remaining elements (due to collisions)
            while(result.Count < totNumber)
            {
                var needed = totNumber - result.Count;
                var newStrings = await CreateRandomStrings(needed, Math.Min(numThreads, needed));
                foreach (var s in newStrings) result.Add(s);
            }

            return result;
        }
    }
}
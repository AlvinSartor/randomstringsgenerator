﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace GenerateStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting to create strings...");
            var stopwatch = Stopwatch.StartNew();
            var result = Parallelizer.CreateRandomStrings(220000000, 100).Result;
            stopwatch.Stop();

            Console.WriteLine($"Elapsed milliseconds: {stopwatch.ElapsedMilliseconds}");
            Console.WriteLine("Writing to file...");
            //SaveStringsToFile(result);  // not used during testing
            Console.WriteLine("Writing complete.");
        }

        private static void SaveStringsToFile(IEnumerable<string> s) =>
            File.WriteAllLines(Path.Combine(SpecialDirectories.Desktop, "randomStrings.txt"), s);
    }
}

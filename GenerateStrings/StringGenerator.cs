﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace GenerateStrings
{
    internal static class StringGenerator
    {
        internal static readonly char[] AdmittedChars = Enumerable.Range(97, 26).Select(x => (char)x).ToArray();
        
        private static char RandomChar(Random rnd) => AdmittedChars[rnd.Next(AdmittedChars.Length)];

        /// <summary>
        /// Generates a random string with a length of 8 characters.
        /// <para>The string characters are in the range ['a','z'].</para>
        /// </summary>
        public static string GenerateString(Random rnd) => 
            new string(Enumerable.Range(0, 8).Select(x => RandomChar(rnd)).ToArray());

        /// <summary>
        /// Generates an array of random, 8 characters strings.
        /// </summary>
        /// <param name="rnd">The random number generator.</param>
        /// <param name="size">The size of the array.</param>
        public static string[] GenerateBatchOfStrings(Random rnd, int size) => 
            Enumerable.Range(0, size).Select(x => GenerateString(rnd)).ToArray();

        /// <summary>
        /// Generates an array of random, 8 characters strings. Performed asynchronously.
        /// </summary>
        /// <param name="rnd">The random number generator.</param>
        /// <param name="size">The size of the array.</param>
        public static async Task<string[]> GenerateBatchOfStringsAsync(Random rnd, int size) =>
            await Task.Run(() => GenerateBatchOfStrings(rnd, size));
    }
}

# RandomStringsGenerator

String generator, made for fun.

I got the idea reading: https://ytec.nl/en/blog/generating-lots-unique-random-strings/
Using some asynchronous programming and smart data structures, the time needed to generate 220 million unique strings is less than 4 minutes:

![strings generation results](Elapsed.png)